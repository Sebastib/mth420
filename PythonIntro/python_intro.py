from __future__ import print_function
from itertools import combinations
import calculator as calc
import numpy as np
#from matplotlib import pyplot as plt
# python_intro.py
"""Python Essentials: Introduction to Python.
<Name>
Benjamin Sebastian
<Class>
MTH 420
<Date>
1/12/18
"""


###########
## LAB 1 ##
###########
print("\nLab 1")

## Problem #1
print("\nProblem #1")
if __name__ == "__main__":
    print("Hello, World")

## Problem #2
print("\nProblem #2")
def sphere_volume(radius):
    return 3.1415926*(4.0/3.0)*radius**3

print(sphere_volume(2.0))

## Problem #3
print("\nProblem #3")
def isolate(a,b,c,d,e):
    print(a,b,c,sep="     ", end=' ')
    print(d,e,sep=" ", end='\n')
    
isolate(1,2,3,4,5)

## Problem #4
print("\nProblem #4")
def first_half(word):
    print(word[0:len(word)/2])

def backward(word):
    print(word[::-1])

first_half("Hello, world")
backward("Hello, world")

## Problem #5
print("\nProblem #5")
def list_ops():
    animals = ["bear","ant","cat","dog"]
    animals.append("eagle")
    animals[2] = "fox"
    del animals[1]
    animals.sort()
    animals.reverse()
    for i in range(len(animals)):
        if animals[i] == "eagle":
            animals[i] = "hawk"
            print(animals)
    animals[-1]+="hunter"
    return animals

animals = list_ops()
print(animals)

## Problem #6
print("\nProblem #6")
def pig_latin(word):
    if word[0] in {'a','e','i','o','u'}:
        return word+'hay'
    else:
        return word[1::]+word[0]+'ay'

print("Original word: animals, Translation: ", end='')
print(pig_latin("animals"))
print("Original word: tractor, Translation: ", end='')
pig_latin("tractor")

## Problem #7
print("\nProblem #7")
def palindrome():
    palindromes = {}
    for a in range(1000):
        for b in range(1000):
            product = a*b
            if str(product) == str(product)[::-1]:
                palindromes[(a,b)] = product
    return max(palindromes, key=palindromes.get)
            
largestPalindromicPair = palindrome()
print(largestPalindromicPair)

## Problem #8
print("\nProblem #8")
def alt_harmonic():
    Sum = 0
    for i in range(1,501):
        Sum += ((-1)**(i+1))*1.0/float(i)
    print(Sum)

alt_harmonic()

###########
## LAB 2 ##
###########
print("\n\nLab 2")

## Problem #1
print("\nProblem #1")
def list_stats(List):
    return max(List),min(List),sum(List)/float(len(List))

L = [1,2,3,4,5,6,7,8,9]
Max,Min,Mean = list_stats(L)
print("Max: %d Min: %d Mean: %f" % (Max,Min,Mean))

## Problem #2
print("\nProblem #2")
#int
int1 = 1
int2 = int1
int2 = 2
if int2 == int1:
    print("ints are mutable\n")
else:
    print("ints are not mutable\n")
#string
string1 = "cow"
string2 = string1
string2 = "horse"
if string2 == string1:
        print("strings are mutable\n")
else:
        print("strings are not mutable\n")
#list
list1 = [1,2,3,4,5]
list2 = list1
list2[0] = 2
if list2 == list1:
        print("lists are mutable\n")
else:
        print("lists are not mutable\n")
#tuple
tuple1 = (2,3)
tuple2 = tuple1
tuple2 = (1,)
if tuple2 == tuple1:
        print("tuples are mutable\n")
else:
        print("tuples are not mutable\n")
#set
set1 = {1,2,3,4,5}
set2 = set1
set2.add(6)
if set2 == set1:
        print("sets are mutable\n")
else:
        print("sets are not mutable\n")

## Problem #3
print("Problem #3")
def calculate_hypotenuse(a,b):
    print(calc.sqrt(calc.Sum(calc.Product(a,a),calc.Product(b,b))))

calculate_hypotenuse(3,4)

## Problem #4
print("\nProblem #4")
def print_power_set(A):
    powerSet = []
    for n in range(len(A)+1):
        powerSet += list(combinations(A, n))
    print(powerSet)

A = [1,2,3,4]
print_power_set(A)

###########
## LAB 3 ##
###########
print("\n\nLab 3")

## Problem #1
print("\nProblem #1")

A = np.array([[3,-1,4],[1,5,-9]])
B = np.array([[2,6,-5,3],[5,-8,9,7],[9,-3,-2,-3]])
AB = np.dot(A,B)
print(AB)

## Problem #2
print("\nProblem #2")
def CayleyHamilton(A):
    return -np.dot(A,np.dot(A,A))+9*np.dot(A,A)-15*A

A = np.array([[3,1,4],[1,5,9],[-5,3,1]])
print(CayleyHamilton(A))

## Problem #3
print("\nProblem #3")

def defineMatrices():
    A = np.triu(np.ones((7,7),dtype=np.int))
    B = -np.tril(np.ones((7,7),dtype=np.int))+5*np.triu(np.ones((7,7),dtype=np.int))-5*np.eye(7)
    return A,B

A,B = defineMatrices()

print((np.dot(A,np.dot(B,A))).astype(np.int64))

## Problem #4
print("\nProblem #4")

def negativeEntriesToZero(A):
    mask = A < 0
    B = A
    B[mask] = 0
    return B

A = np.array([[-1,-2,-3],[1,2,3],[4,5,6]])
A_withoutZeros = negativeEntriesToZero(A)

## Problem #5
print("\nProblem #5")

def createMatrices():
    A = np.array([[0,2,4],[1,3,5],[0,0,0]])
    B = np.array([[3,0,0],[3,3,0],[3,3,3]])
    C = np.array([[-2,0,0],[0,-2,0],[0,0,-2]])
    return A,B,C

A,B,C = createMatrices()
Z = np.zeros((3,3))
I = np.identity(3)
blockMatrix = np.vstack((np.hstack((Z,A.T,I)),np.hstack((A,Z,Z)),np.hstack((B,Z,C))))

print(blockMatrix)

## Problem #6
print("\nProblem #6")

def rowStochastic(A):
    return A/(A.sum(axis=1)[:,None])

A = np.array([[0.,1.,2.],[3.,4.,5.],[6.,7.,8.]])

print(rowStochastic(A))

###########
## LAB 4 ##
###########
print("\n\nLab #4")

## Problem #1
print("\nProblem #1")

def calculateVariances(n):
    x = np.random.normal(size=(n,n))
    means = np.mean(x,axis=1)
    return np.var(means)

def getVars():
    variances = []
    for n in xrange(1,11):
        variances.append(calculateVariances(100*n))
    return variances

#plt.plot(np.arange(10),getVars())
#plt.show()

## Problem #2
print("\nProblem #2")

x = np.arange(-2*np.pi,2*np.pi,.1)
#plt.plot(x,np.sin(x),'r')
#plt.plot(x,np.cos(x),'b')
#plt.plot(x,np.arctan(x),'k')
