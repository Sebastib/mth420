from __future__ import print_function
import numpy as np
# numpy_intro.py
"""Python Essentials: Intro to NumPy.
<Name> Benjamin Sebastian
<Class> MTHh 420
<Date>
"""


def prob1():
    """Define the matrices A and B as arrays. Return the matrix product AB."""
    A = np.array([[3,-1,4],[1,5,-9]])
    B = np.array([[2,6,-5,3],[5,-8,9,7],[9,-3,-2,-3]])
    AB = np.dot(A,B)
    return AB


def prob2():
    """Define the matrix A as an array. Return the matrix -A^3 + 9A^2 - 15A."""
    A = np.array([[3,1,4],[1,5,9],[-5,3,1]])
    return -np.dot(A,np.dot(A,A))+9*np.dot(A,A)-15*A


def prob3():
    """Define the matrices A and B as arrays. Calculate the matrix product ABA,
    change its data type to np.int64, and return it.
    """
    A = np.triu(np.ones((7,7),dtype=np.int))
    B = -np.tril(np.ones((7,7),dtype=np.int))+5*np.triu(np.ones((7,7),dtype=np.int))-5*np.eye(7)
    ABA = (np.dot(A,np.dot(B,A))).astype(np.int64)
    return ABA


def prob4(A):
    """Make a copy of 'A' and set all negative entries of the copy to 0.
    Return the copy.

    Example:
        >>> A = np.array([-3,-1,3])
        >>> prob4(A)
        array([0, 0, 3])
    """
    mask = A < 0
    B = A
    B[mask] = 0
    return B


def prob5():
    """Define the matrices A, B, and C as arrays. Return the block matrix
                                | 0 A^T I |
                                | A  0  0 |,
                                | B  0  C |
    where I is the 3x3 identity matrix and each 0 is a matrix of all zeros
    of the appropriate size.
    """
    A = np.array([[0,2,4],[1,3,5],[0,0,0]])
    B = np.array([[3,0,0],[3,3,0],[3,3,3]])
    C = np.array([[-2,0,0],[0,-2,0],[0,0,-2]])
    Z = np.zeros((3,3))
    I = np.identity(3)
    blockMatrix = np.vstack((np.hstack((Z,A.T,I)),np.hstack((A,Z,Z)),np.hstack((B,Z,C))))
    return blockMatrix


def prob6(A):
    """Divide each row of 'A' by the row sum and return the resulting array.

    Example:
        >>> A = np.array([[1,1,0],[0,1,0],[1,1,1]])
        >>> prob6(A)
        array([[ 0.5       ,  0.5       ,  0.        ],
               [ 0.        ,  1.        ,  0.        ],
               [ 0.33333333,  0.33333333,  0.33333333]])
    """
    return A/(A.sum(axis=1)[:,None])


def prob7():
    """Given the array stored in grid.npy, return the greatest product of four
    adjacent numbers in the same direction (up, down, left, right, or
    diagonally) in the grid.
    """
    raise NotImplementedError("Problem 7 Incomplete")
