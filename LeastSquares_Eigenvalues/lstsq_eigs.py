from __future__ import print_function
import numpy as np
from matplotlib import pyplot as plt
# lstsq_eigs.py
"""Volume 1: Least Squares and Computing Eigenvalues.
<Name> Benjamin Sebastian
<Class> MTH420
<Date>
"""

# (Optional) Import functions from your QR Decomposition lab.
# import sys
# sys.path.insert(1, "../QR_Decomposition")
# from qr_decomposition import qr_gram_schmidt, qr_householder, hessenberg


# Problem 1
def least_squares(A, b):
    """Calculate the least squares solutions to Ax = b by using the QR
    decomposition.

    Parameters:
        A ((m,n) ndarray): A matrix of rank n <= m.
        b ((m, ) ndarray): A vector of length m.

    Returns:
        x ((n, ) ndarray): The solution to the normal equations.
    """
    Q,R = np.linalg.qr(A)
    return np.dot(np.dot(np.linalg.inv(R),Q.T),b)

# Problem 2
def line_fit():
    """Find the least squares line that relates the year to the housing price
    index for the data in housing.npy. Plot both the data points and the least
    squares line.
    """
    HPI = np.load("housing.npy")
    A = []
    b = []
    for i in range(len(HPI)):
        A.append([HPI[i][0],1])
        b.append(HPI[i][1])
    LS_sol = least_squares(A,b)
    x = np.arange(0,HPI[len(HPI)-1][0],.05)
    plt.plot(HPI[:,0],HPI[:,1],'ro')
    plt.plot(x,LS_sol[0]*x+LS_sol[1],'b-')
    plt.show()

# Problem 3
from scipy import linalg as la
def polynomial_fit():
    """Find the least squares polynomials of degree 3, 6, 9, and 12 that relate
    the year to the housing price index for the data in housing.npy. Plot both
    the data points and the least squares polynomials in individual subplots.
    """
    '''def constructAb(n,array):
        A = []
        for i in range(len(array)):
            row = []
            for j in range(n+1):
                row.append(array[i][0]**j)
            A.append(row)
        b = array[:,1]
        return A,b'''

def polynomial(N,x,c):
        y = []
        Sum = 0
        for i in range(len(x)):
            for j in range(N,-1,-1):
                Sum += c[j]*(x[i]**j)
            y.append(Sum)
        return y
            
    HPI = np.load("housing.npy")
    x_data = HPI[:,0]
    b = HPI[:,1]
    x_poly = np.arange(min(x_data),max(x_data),.05)
    
    plt.subplot(221)
    A3 = np.vander(x_data,4)
    c = la.lstsq(A3,b)[0]
    y = polynomial(3,x_poly,c)
    plt.plot(x_data,b,'ko')
    plt.plot(x_poly,y,'r-')
    plt.title("3rd degree polynomial LS fit")
    
    plt.subplot(222)
    A6 = np.vander(x_data,7)
    c = la.lstsq(A6,b)[0]
    y = polynomial(6,x_data,c)
    plt.plot(x_data,b,'ko')
    plt.plot(x_poly,y,'r-')
    plt.title("6th degree polynomial LS fit")

    plt.subplot(223)
    A9 = np.vander(x,10)
    c = la.lstsq(A9,b)[0]
    y = polynomial(9,x_poly,c)
    plt.plot(x_data,b,'ko')
    plt.plot(x_poly,y,'r-')
    plt.title("9th degree polynomial LS fit")

    plt.subplot(224)
    A12 = np.vander(x,13)
    c = la.lstsq(A12,b)[0]
    y = polynomial(12,x_poly,c)
    plt.plot(x_data,b,'ko')
    plt.plot(x_poly,y,'r-')
    plt.title("12th degree polynomial LS fit")
    
    plt.tight_layout()
    plt.show()

def plot_ellipse(a, b, c, d, e):
    """Plot an ellipse of the form ax^2 + bx + cxy + dy + ey^2 = 1."""
    theta = np.linspace(0, 2*np.pi, 200)
    cos_t, sin_t = np.cos(theta), np.sin(theta)
    A = a*(cos_t**2) + c*cos_t*sin_t + e*(sin_t**2)
    B = b*cos_t + d*sin_t
    r = (-B + np.sqrt(B**2 + 4*A)) / (2*A)

    plt.plot(r*cos_t, r*sin_t)
    plt.gca().set_aspect("equal", "datalim")

# Problem 4
def ellipse_fit():
    """Calculate the parameters for the ellipse that best fits the data in
    ellipse.npy. Plot the original data points and the ellipse together, using
    plot_ellipse() to plot the ellipse.
    """
    raise NotImplementedError("Problem 4 Incomplete")


# Problem 5
def power_method(A, N=20, tol=1e-12):
    """Compute the dominant eigenvalue of A and a corresponding eigenvector
    via the power method.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        N (int): The maximum number of iterations.
        tol (float): The stopping tolerance.

    Returns:
        (float): The dominant eigenvalue of A.
        ((n,) ndarray): An eigenvector corresponding to the dominant
            eigenvalue of A.
    """
    raise NotImplementedError("Problem 5 Incomplete")


# Problem 6
def qr_algorithm(A, N=50, tol=1e-12):
    """Compute the eigenvalues of A via the QR algorithm.

    Parameters:
        A ((n,n) ndarray): A square matrix.
        N (int): The number of iterations to run the QR algorithm.
        tol (float): The threshold value for determining if a diagonal S_i
            block is 1x1 or 2x2.

    Returns:
        ((n,) ndarray): The eigenvalues of A.
    """
    raise NotImplementedError("Problem 6 Incomplete")
