from __future__ import print_function
from itertools import combinations
import calculator as calc
# standard_library.py
"""Python Essentials: The Standard Library.
<Name> Benjamin Sebastian
<Class> MTH 420
<Date>
"""


# Problem 1
def prob1(L):
    """Return the minimum, maximum, and average of the entries of L
    (in that order).
    """
    return max(L),min(L),sum(L)/float(len(L))
    
# Problem 2
def prob2():
    """Determine which Python objects are mutable and which are immutable.
    Test numbers, strings, lists, tuples, and sets. Print your results.
    """
    #int     
    int1 = 1
    int2 = int1
    int2 = 2
    if int2 == int1:
        print("ints are mutable\n")
    else:
        print("ints are not mutable\n")
    #string                                                                   
    string1 = "cow"
    string2 = string1
    string2 = "horse"
    if string2 == string1:
        print("strings are mutable\n")
    else:
        print("strings are not mutable\n")
    #list
    list1 = [1,2,3,4,5]
    list2 = list1
    list2[0] = 2
    if list2 == list1:
        print("lists are mutable\n")
    else:
        print("lists are not mutable\n")
    #tuple
    tuple1 = (2,3)
    tuple2 = tuple1
    tuple2 = (1,)
    if tuple2 == tuple1:
        print("tuples are mutable\n")
    else:
        print("tuples are not mutable\n")
    #set
    set1 = {1,2,3,4,5}
    set2 = set1
    set2.add(6)
    if set2 == set1:
        print("sets are mutable\n")
    else:
        print("sets are not mutable\n")


# Problem 3
def hypot(a, b):
    """Calculate and return the length of the hypotenuse of a right triangle.
    Do not use any functions other than those that are imported from your
    'calculator' module.

    Parameters:
        a: the length one of the sides of the triangle.
        b: the length the other non-hypotenuse side of the triangle.
    Returns:
        The length of the triangle's hypotenuse.
    """
    return calc.sqrt(calc.Sum(calc.Product(a,a),calc.Product(b,b)))

# Problem 4
def power_set(A):
    """Use itertools to compute the power set of A.

    Parameters:
        A (iterable): a str, list, set, tuple, or other iterable collection.

    Returns:
        (list(sets)): The power set of A as a list of sets.
    """
    powerSet = []
    for n in range(len(A)+1):
        powerSet += set(combinations(A, n))
    return powerSet

# Problem 5: Implement shut the box.

